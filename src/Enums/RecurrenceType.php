<?php

namespace Helium\CalendarStore\Enums;

use Konekt\Enum\Enum;

class RecurrenceType extends Enum
{
	const __DEFAULT = self::NULL;

	const NULL = null;
	const YEARLY = 'Yearly';
	const MONTHLY = 'Monthly';
	const WEEKLY = 'Weekly';
	const DAILY = 'Daily';

	public static function all()
	{
		return array_filter(self::values());
	}
}