<?php

namespace Helium\CalendarStore\Models;

use Carbon\Carbon;
use Helium\CalendarStore\Enums\RecurrenceType;
use Helium\EloquentBaseModels\FlexModel;
use Illuminate\Support\Collection;
use Iterator;

/**
 * Class CalendarEvent
 *
 * @property string id
 * @property Carbon created_at
 * @property Carbon updated_at
 * @property Carbon deleted_at
 * @property string title
 * @property string description
 * @property bool all_day
 * @property Carbon start_date
 * @property Carbon start_time
 * @property Carbon end_date
 * @property Carbon end_time
 *
 * Flex Fields
 * @property string parent_id
 * @property int recurrence_sequence_number
 * @property RecurrenceType recurrence_type
 * @property Carbon recurrence_end
 * @property int recurrence_frequency
 * @property array recurrence_days
 *
 * Relationships
 * @property CalendarEvent parent
 * @property Collection instances
 *
 * Calculated Fields
 * @property bool recurs
 *
 * Local properties
 * @property CalendarEvent current
 * @property int current_day_index
 */
class CalendarEvent extends FlexModel implements Iterator
{
	public $current;
	protected $current_day_index;

	protected $attributes = [
		'all_day' => false
	];

	protected $fillable = [
		'title',
		'description',
		'all_day',
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'parent_id',
		'recurrence_sequence_number',
		'recurrence_type',
		'recurrence_end',
		'recurrence_frequency',
		'recurrence_days'
	];

	protected $baseAttributes = [
		'id',
		'created_at',
		'updated_at',
		'deleted_at',
		'title',
		'description',
		'all_day',
		'start_date',
		'start_time',
		'end_date',
		'end_time'
	];

	protected $dates = [
		'created_at',
		'updated_at',
		'deleted_at',
		'start_date',
		'start_time',
		'end_date',
		'end_time',
		'recurrence_end'
	];

	protected $enums = [
		'recurrence_type' => RecurrenceType::class
	];

	public function rules()
	{
		$baseRules = [
			'title' => 'required|string',
			'description' => 'nullable|string',
			'all_day' => 'required|boolean',
			'start_date' => 'required|before_or_equal:end_date',
			'end_date' => 'required|after_or_equal:start_date',
		];

		$flexRules = [];

		if ($this->start_time || $this->end_time)
		{
			$flexRules['start_time'] = 'required|before:end_time';
			$flexRules['end_time'] = 'required|after:end_time';
		}

		if ($this->parent_id)
		{
			$flexRules['parent_id'] = 'required';//|id:CalendarEvent';
			$flexRules['recurrence_sequence_number'] = 'required|int|min:1';

			//TODO: Forbidden rules
		}

		if ($this->recurrence_type->value())
		{
			$enum_values = implode(',', RecurrenceType::all());
			$flexRules['recurrence_type'] = 'required|in:' . $enum_values;
			$flexRules['recurrence_end'] = 'nullable|after:start_date';
			$flexRules['recurrence_frequency'] = 'required|int|min:1';

			if ($this->recurrence_type == RecurrenceType::WEEKLY ||
				$this->recurrence_type == RecurrenceType::MONTHLY)
			{
				$flexRules['recurrence_days'] = 'required|array';
			}
		}

		return array_merge($baseRules, $flexRules);
	}

	public function getRecursAttribute()
	{
		return !is_null($this->recurrence_type->value());
	}

	public function parent()
	{
		return $this->belongsTo(CalendarEvent::class, 'parent_id');
	}

	public function instances()
	{
		return $this->hasMany(CalendarEvent::class, 'data->parent_id');
	}

	public function expand(Carbon $window_start, Carbon $window_end): Collection
	{
		if (!$this->recurrence_end)
		{
			$this->recurrence_end = $window_end;
		}

		//If the requested window does not overlap with the recurrence window,
		//return an empty collection
		if ($window_start > $this->recurrence_end || $window_end < $this->start_date)
		{
			return collect([]);
		}

		$instances = collect(iterator_to_array($this))->filter(
			function($instance) use ($window_start, $window_end) {
				/** @var CalendarEvent $instance */
				return $instance->start_date >= $window_start &&
					$instance->end_date <= $window_end;
			});

		return $instances;
	}

	public function current()
	{
		if ($this->valid())
		{
			return $this->current;
		}
	}

	public function key()
	{
		return $this->current->recurrence_sequence_number;
	}

	protected function nextStartForType(string $recurrenceType)
	{
		$index = $this->current_day_index;
		$next_period = false;

		if (++$index == count($this->recurrence_days))
		{
			$index = 0;
			$next_period = true;
		}

		$day = $this->recurrence_days[$index];
		$month = $this->current->start_date->month;
		$year = $this->current->start_date->year;

		switch ($recurrenceType)
		{
			case RecurrenceType::WEEKLY:

				$start_date = date('l', strtotime("Sunday + $day days"));

				$current = $this->current->start_date->toString();
				$start_date = Carbon::create("$current next $start_date");

				if ($next_period)
				{
					$start_date = $start_date->addweeks(
						$this->recurrence_frequency - 1);
				}

				break;
			case RecurrenceType::MONTHLY:
				$current_day = $this->recurrence_days[$this->current_day_index];

				$add_years = 0;

				if ($day <= $current_day)
				{
					$month = ($month % 12) + 1;
					$add_years += $month == 1 ? 1 : 0;
				}

				if ($next_period)
				{
					$add_years += (($month - 1 + $this->recurrence_frequency))
						/ 12;
					$month = (($month - 1 + ($this->recurrence_frequency - 1))
							% 12) + 1;
				}

				$year = floor($year + $add_years);

				$days_in_month = Carbon::create("$year/$month/01")->daysInMonth;

				if ($days_in_month < $day)
				{
					$day = $days_in_month;
				}

				$start_date = Carbon::create("$year/$month/$day");

				break;
			case RecurrenceType::YEARLY:
				$current_day = Carbon::create($this->recurrence_days[$this->current_day_index]);
				$start_date = Carbon::create($day);

				$add_years = 0;

				if ($next_period)
				{
					$add_years += $this->recurrence_frequency - 1;
				}

				if ($start_date <= $current_day)
				{
					$add_years += 1;
				}

				$start_date->setYear($this->current->start_date->year + $add_years);

				break;
		}

		$this->current_day_index = ($this->current_day_index + 1) %
			count($this->recurrence_days);

		return $start_date;
	}

	public function next()
	{
		if ($this->current === null)
		{
			$this->rewind();
		} elseif (!$this->valid())
		{
			return null;
		}
		else
		{
			switch ($this->recurrence_type)
			{
				case RecurrenceType::DAILY:
					$start_date = $this->current->start_date
						->addDays($this->recurrence_frequency);

					break;
				case RecurrenceType::WEEKLY:
				case RecurrenceType::MONTHLY:
				case RecurrenceType::YEARLY:
					$start_date = $this->nextStartForType($this->recurrence_type);
					break;
			}

			$diff = $this->start_date->diff($this->end_date);
			$end_date = $start_date->copy()->add($diff);

			$this->current = new CalendarEvent([
				'title' => $this->title,
				'description' => $this->description,
				'all_day' => $this->all_day,
				'start_date' => $start_date,
				'start_time' => $this->start_time,
				'end_date' => $end_date,
				'end_time' => $this->end_time,
				'parent_id' => $this->id,
				'recurrence_sequence_number' =>
					$this->current->recurrence_sequence_number + 1
			]);
		}

		if ($instance = $this->instances->where('recurrence_sequence_number',
			$this->current->recurrence_sequence_number)->first())
		{
			return $instance;
		}

		return $this->current();
	}

	public function rewind()
	{
		if (!$this->recurs)
		{
			$this->current = $this;
		}
		else if ($first = $this->instances->where('recurrence_sequence_number', 0)->first())
		{
			$this->current = $first;
		}
		else
		{
			$this->current = new CalendarEvent([
				'title' => $this->title,
				'description' => $this->description,
				'all_day' => $this->all_day,
				'start_date' => $this->start_date,
				'start_time' => $this->start_time,
				'end_date' => $this->end_date,
				'end_time' => $this->end_time,
				'parent_id' => $this->id,
				'recurrence_sequence_number' => 0
			]);

			switch ($this->recurrence_type)
			{
				case RecurrenceType::WEEKLY:
					$this->current_day_index = array_search(
							$this->start_date->dayOfWeek,
							$this->recurrence_days
						) ?? 0;
					break;
				case RecurrenceType::MONTHLY:
					$this->current_day_index = array_search(
							$this->start_date->day,
							$this->recurrence_days
						) ?? 0;
					break;
				case RecurrenceType::YEARLY:
					$this->current_day_index = array_search(
							$this->start_date->format('M d'),
							$this->recurrence_days
						) ?? 0;
					break;
			}
		}
	}

	public function valid()
	{
		$valid = true;

		if (!$this->recurs)
		{
			$valid = $valid && $this->current == $this;
		}

		if ($this->recurrence_start)
		{
			$valid = $valid && $this->current->start_date > $this->recurrence_start;
		}

		if ($this->recurrence_end)
		{
			$valid = $valid && $this->current->start_date < $this->recurrence_end;
		}
		else
		{
			$valid = $valid && $this->current->recurrence_sequence_number < 100;
		}

		return $valid;
	}
}