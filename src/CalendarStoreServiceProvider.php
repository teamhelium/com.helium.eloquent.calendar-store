<?php

use Illuminate\Support\ServiceProvider;

class CalendarStoreServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->registerMigrations();
		$this->registerPublishing();
	}

	/**
	 * Register the package migrations.
	 *
	 * @return void
	 */
	protected function registerMigrations()
	{
		$this->loadMigrationsFrom(__DIR__ . '/Migrations');
	}

	/**
	 * Register the package's publishable resources.
	 *
	 * @return void
	 */
	protected function registerPublishing()
	{
		if ($this->app->runningInConsole()) {
			$this->publishes([
				__DIR__ . '/Migrations' => '/database/migrations',
			]);
		}
	}
}
