<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarEventsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_events', function (Blueprint $table) {
			$table->string('id')->primary();
			$table->timestamps();
			$table->softDeletes();
			$table->string('title');
			$table->string('description')->nullable();
			$table->boolean('all_day')->default(false);
			$table->date('start_date')->nullable();
			$table->time('start_time')->nullable();
			$table->date('end_date')->nullable();
			$table->time('end_time')->nullable();
			$table->json('data'); //FlexModel data
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('forms');
	}
}
